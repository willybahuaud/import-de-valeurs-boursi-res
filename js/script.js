jQuery(document).ready(function ($) { 
    // Convert data to int
    var data = [];
    $.each( dataNav, function( i, val ) {
        var t = val[0].substr(0,10).split('-');
        data.push( [ Date.UTC( t[0], t[1]-1, t[2] ), parseFloat( val[1] ) ] );
    } );
    $('#iv').highcharts( 'StockChart', {
        /*chart: {
            type: 'spline'
        },
        title: {
            text: 'Évolution EUR institutionnel'
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%e. %b',
                year: '%b'
            }
        },
        yAxis: {
            title: {
                text: 'Valeur NAV'
            }
        },*/
        rangeSelector : {
            selected : 1
        },
        series: [{
            name: 'EUR institutionnel',
            data: data
        }]/*,
        tooltip: {
            formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                    Highcharts.dateFormat('%e. %b', this.x) +': '+ this.y;
            }
        }*/
    });
});