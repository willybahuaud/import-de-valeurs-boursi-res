<?php 
/*
Plugin Name: Import valeurs
Description: Importe les valeurs quotidiennes de l'API yahoo finance
Version: 0.9
Author: willybahuaud
Author URI: http://wabeo.fr
Text Domain: nebula
Domain Path: /languages
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

/**
Valeurs
* EUR private : IE00B5TWCG10.IR
* EUR institutional : IE00B7VNF787.IR
* USD institutional : IE00B7L3NC99.IR
* USD private : IE00B7V62D22.IR
* CHF institutional : IE00B58WVS70.IR
* CHF private :IE00B82WVM84.IR
* CHF select institutional : IE00B8KYH732.IR
* EUR select Institutional : IE00B8FLYX43.IR
*/
function iv_get_values_names() {
	return array(
					'EUR Retail Class'        => 'IE00B5TWCG10.IR',
					'EUR Institutional Class' => 'IE00B7VNF787.IR',
					'USD Institutional Class' => 'IE00B7L3NC99.IR',
					'USD Retail Class'        => 'IE00B7V62D22.IR',
					'CHF Institutional Class' => 'IE00B58WVS70.IR',
					'CHF Retail Class'        => 'IE00B82WVM84.IR',
					'CHF Select Class'        => 'IE00B8KYH732.IR',
					'EUR Select Class'        => 'IE00B8FLYX43.IR'
	             );
}
/**

Flux
* http://developer.yahoo.com/yql/console/?q=SELECT%20*%20FROM%20yahoo.finance.historicaldata%20WHERE%20symbol%3D'IE00B7VNF787.IR'%20and%20startDate%20%3D%20'2014-01-23'%20and%20endDate%20%3D%20'2014-01-24'&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys
* http://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20FROM%20yahoo.finance.historicaldata%20WHERE%20symbol%3D%27IE00B7VNF787.IR%27%20and%20startDate%20%3D%20%272014-01-23%27%20and%20endDate%20%3D%20%272014-01-24%27&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=
*/

add_action( 'init', 'iv_register_contents' );
function iv_register_contents() {
	$labels = array(
		'name'				=> _x( 'values', 'post type general name', 'nebula' ),
		'singular_name'		=> _x( 'value', 'post type singular name', 'nebula' ),
		'menu_name'			=> _x( 'values', 'post type general name', 'nebula' ),
		'all_items'			=> __( 'All values', 'nebula' ),
		'add_new'			=> _x( 'Add New', 'value', 'nebula' ),
		'add_new_item'		=> __( 'Add New value', 'nebula' ),
		'edit_item'			=> __( 'Edit value', 'nebula' ),
		'new_item'			=> __( 'New value', 'nebula' ),
		'view_item'			=> __( 'View value', 'nebula' ),
		'items_archive'		=> _x( 'values', 'post type general name', 'nebula' ),
		'search_items'		=> __( 'Search values', 'nebula' ),
		'not_found'			=> __( 'No values found.', 'nebula' ),
		'not_found_in_trash'=> __( 'No values found in trash.', 'nebula' ),
		'parent_item_colon'	=> __( 'Parent value:', 'nebula' ),
	);
	register_post_type( 'value', array(
		'label' 		=> _x( 'values', 'post type general name', 'nebula' ),
		'labels'		=> $labels,
		'public'		=> true
		// etc.
	) );

	$labels = array(
		'name'							=> _x( 'values names', 'taxonomy general name', 'nebula' ),
		'singular_name'					=> _x( 'value name', 'taxonomy singular name', 'nebula' ),
		'menu_name'						=> _x( 'values names', 'taxonomy general name', 'nebula' ),
		'search_items'					=> __( 'Search values names', 'nebula' ),
		'popular_items'					=> __( 'Popular values names', 'nebula' ),
		'all_items'						=> __( 'All values names', 'nebula' ),
		'parent_item'					=> __( 'Parent value name', 'nebula' ),
		'parent_item_colon'				=> __( 'Parent value name:', 'nebula' ),
		'edit_item'						=> __( 'Edit value name', 'nebula' ),
		'view_item'						=> __( 'View value name', 'nebula' ),
		'update_item'					=> __( 'Update value name', 'nebula' ),
		'add_new_item'					=> __( 'Add New value name', 'nebula' ),
		'new_item_name'					=> __( 'New value name Name', 'nebula' ),
		'separate_items_with_commas' 	=> __( 'Separate values names with commas', 'nebula' ),
		'add_or_remove_items'			=> __( 'Add or remove values names', 'nebula' ),
		'choose_from_most_used'			=> __( 'Choose from the most used values names', 'nebula' ),
		'not_found'						=> __( 'No values names found.', 'nebula' ),
	);
	register_taxonomy( 'type_value', 'value', array(
		'label' 		=> _x( 'values names', 'taxonomy general name', 'nebula' ),
		'labels'		=> $labels,
		'public'		=> true
		// etc.
	) );
}

add_filter( 'post_updated_messages', 'value_post_type_messages' );
function value_post_type_messages( $messages ) {
	global $post;
	if ( $post->post_type !== 'value' )
		return $messages;

	$post_url		= get_permalink($post->ID);
	$preview_url 	= esc_url( add_query_arg( 'preview', 'true', $post_url ) );
	$post_url		= esc_url( $post_url );

	$messages[$post->post_type] = array(
		 0 => '', // Unused. Messages start at index 1.
		 1 => sprintf( __( 'value updated. <a href=\'%s\'>View value</a>', 'nebula' ), $post_url ),
		 2 => __( 'Custom field updated.' ),
		 3 => __( 'Custom field deleted.' ),
		 4 => __( 'value updated.', 'nebula' ),
		/* translators: %s: date and time of the revision */
		 5 => isset($_GET['revision']) ? sprintf( __( 'value restored to revision from %s', 'nebula' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		 6 => sprintf( __( 'value published. <a href=\'%s\'>View value</a>', 'nebula' ), $post_url ),
		 7 => __( 'value saved.', 'nebula' ),
		 8 => sprintf( __( 'value submitted. <a target=\'_blank\' href=\'%s\'>Preview value</a>', 'nebula' ), $preview_url ),
		 9 => sprintf( __( 'value scheduled for: <strong>%1$s</strong>. <a target=\'_blank\' href=\'%2$s\'>Preview value</a>', 'nebula' ),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), $post_url ),
		10 => sprintf( __( 'value draft updated. <a target=\'_blank\' href=\'%s\'>Preview value</a>', 'nebula' ), $preview_url ),
	);

	return $messages;
}

add_filter( 'term_updated_messages', 'type_value_taxonomy_messages' );
function type_value_taxonomy_messages( $messages ) {
	$messages['type_value'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'value name added.', 'nebula' ),
		2 => __( 'value name deleted.', 'nebula' ),
		3 => __( 'value name updated.', 'nebula' ),
		4 => __( 'value name not added.', 'nebula' ),
		5 => __( 'value name not updated.', 'nebula' ),
		6 => __( 'values names deleted.', 'nebula' ),
	);

	return $messages;
}


add_filter( 'bulk_post_updated_messages', 'bulk_value_post_type_messages', 10, 2 );
function bulk_value_post_type_messages( $bulk_messages, $bulk_counts ) {
	$bulk_messages['value'] = array(
		'updated'	=> _n( '%s value updated.', '%s values updated.', $bulk_counts['updated'], 'nebula' ),
		'locked' 	=> _n( '%s value not updated, somebody is editing it.', '%s values not updated, somebody is editing them.', $bulk_counts['locked'], 'nebula' ),
		'deleted'	=> _n( '%s value permanently deleted.', '%s values permanently deleted.', $bulk_counts['deleted'], 'nebula' ),
		'trashed'	=> _n( '%s value moved to the Trash.', '%s values moved to the Trash.', $bulk_counts['trashed'], 'nebula' ),
		'untrashed'	=> _n( '%s value restored from the Trash.', '%s values restored from the Trash.', $bulk_counts['untrashed'], 'nebula' ),
	);

	return $bulk_messages;
}

/**
Get values
*/
function iv_import_values( $from, $to ) {
	$result = array();
	$values_names = iv_get_values_names();
	foreach( $values_names as $k => $vn ) {
		$out = iv_retrieve_value_by_name( $vn, $from, $to );
		if ( $out ) {
			$result = array_merge( $out, $result );
		}
	}
	return iv_insert_values( $result );
}

function iv_retrieve_value_by_name( $name, $from, $to ) {
	//YYYY-MM-DD
	$args = array( 'timeout' => 60 );
	$q = wp_remote_get( 'http://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20FROM%20yahoo.finance.historicaldata%20WHERE%20symbol%3D%27' . $name . '%27%20and%20startDate%20%3D%20%27' . $from . '%27%20and%20endDate%20%3D%20%27' . $to . '%27&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=', $args );
	if ( ! is_wp_error( $q ) && 200 == wp_remote_retrieve_response_code( $q ) ) {
		$out = json_decode( wp_remote_retrieve_body( $q ) );
		if ( ! is_null( $out->query->results ) ) {
			$output = array();
			foreach( $out->query->results->quote as $entries ) {
				$output[ $name ][ $entries->Date ] = $entries->Close;
			}
			return $output;
		}
	} 
	return false;
}

function iv_insert_values( $values ) {
	$success = array();
	$errors = array();
	foreach( $values as $k => $val ) {
		foreach( $val as $key => $valeur ) {
			//filtre pour enlever ce qui existe -> nom
			$nom = $k . '-' . $key;
			if( is_null( get_page_by_title( $nom, OBJECT, 'value' ) ) ) {
				$date = $key . ' 00:00:00';
				$postarr = array(
									'post_content'  => $valeur,
									'post_title'    => $nom,
									'post_type'     => 'value',
									'post_status'   => 'publish',
									'post_date'     => $date,
									'post_date_gmt' => $date,
									'post_author'   => 1
				                 );
				$post = wp_insert_post( $postarr, true );
				if ( is_wp_error( $post ) ) {
					$errors[] = $post;
				} else {
					wp_set_post_terms( $post, $k, 'type_value', true );
					$success[] = $post;
				}
			}
		}
	}
	return array( 'insertions' => $success, 'erreurs' => $errors );
}

add_action( 'wp', 'iv_setup_3days_values' );
function iv_setup_3days_values() {
	if ( ! wp_next_scheduled( 'iv_import_3days_values' ) ) {
		wp_schedule_event( time(), 'daily', 'iv_import_3days_values');
	}
}

add_action( 'iv_import_3days_values', 'iv_do_import_tree_days_values' );
function iv_do_import_tree_days_values() {
	$three_days_ago = date( "Y-m-d", strtotime( '-3 days' ) );
	$two_days_ago   = date( "Y-m-d", strtotime( '-2 days' ) );
	$r = iv_import_values( $three_days_ago, $two_days_ago );
	wp_mail( 'w.bahuaud@gmail.com', 'Import du ' . $three_days_ago, return_var_dump( $r ) );
}

/**
Charts
*/
add_action( 'wp_enqueue_scripts', 'iv_register_highcharts' );
function iv_register_highcharts() {
	wp_register_script( 'highcharts', plugins_url( 'js/highcharts.js', __FILE__ ), 'jquery', '3.0.9', true );
	wp_register_script( 'highstock', plugins_url( 'js/highstock.js', __FILE__ ), 'jquery', '3.0.9', true );
	wp_register_script( 'script-iv', plugins_url( 'js/script.js', __FILE__ ), 'highstock', '0.9', true );
}

add_shortcode( 'val', 'test_val' );
function test_val() {
	wp_enqueue_script( 'jquery' );
	// wp_enqueue_script( 'highcharts' );
	wp_enqueue_script( 'highstock' );
	wp_enqueue_script( 'script-iv' );

	// $three_days_ago = date( "Y-m-d", strtotime( '-3 days' ) );
	// $two_days_ago   = date( "Y-m-d", strtotime( '-2 days' ) );
	// $r = iv_import_values( $three_days_ago, $two_days_ago );

	$values = new WP_Query( array(
							'post_type'      => 'value',
							'posts_per_page' => -1,
							'tax_query'      => array(
							                          array(
							                                'taxonomy' => 'type_value',
							                                'terms'    => 'IE00B7VNF787.IR',
							                                'field'    => 'slug'
							                                )
							                          ),
							// 'date_query'     => array(
							// 						array(
							// 							'column' => 'post_date_gmt',
							// 							'after'  => '1 month ago',
							// 						),
							// 					),
							'order'          => 'ASC'
	                       ) );
	$data = array();
	if ( $values->have_posts() ) : while ( $values->have_posts() ) : $values->the_post();
		$data[] = array( $values->post->post_date, $values->post->post_content );
	endwhile; endif;

	wp_localize_script( 'script-iv', 'dataNav', $data );
	$out = '<div id="iv" style="width:100%; height:400px;"></div>';
	return $out;
}

add_shortcode( 'parts', 'iv_show_parts' );
function iv_show_parts() {
	$out = '<table id="iv-parts" class="iv-parts">';
	$out .= '<thead>';
	$out .= '<tr><th colspan="2">Parts</th><th>VNI</th><th>% (1 jour)</th><th>% (1 YTD)</th><th>% (since start)</th></tr>';
	$out .= '<thead>';
	$out .= '<tbody>';
	$names = iv_get_values_names();
	foreach( $names as $k => $name ) {
		$out .= '<tr>';
		$out .= '<th>' . $k . '</th>';
		$out .= '<td>' . str_replace( '.IR', '', $name ) . '</td>';
		$lastvals = get_posts( array( 
		                    'post_type' => 'value', 
		                    'tax_query' => array(
		                                         array(
			                                         'taxonomy' => 'type_value',
			                                         'field'    => 'slug',
			                                         'terms'    => $name
			                                         )
		                                         ),
		                    'posts_per_page' => 2,
		                    'order'     => 'DESC'
		                    ) );
		if( ! empty( $lastvals ) ) {
			$out .= '<td>' . $lastvals[0]->post_content . '</td>';
		} else {
			$out .= '<td>' . __( 'inconnue' ) . '</td>';
		}
		$out .= '<td>' . iv_get_1daydelta( $name, $lastvals ) . '</td>';
		$out .= '<td>' . iv_get_1yeardelta( $name, $lastvals ) . '</td>';
		$out .= '<td>' . iv_get_totaldelta( $name, $lastvals ) . '</td>';
		$out .= '</tr>';
	}
	$out .= '</tbody>';
	$out .= '</table>';
	return $out;
}

function iv_get_1daydelta( $partname, $lastvals ) {
	if ( ! isset( $lastvals[1] ) ) {
		return __( 'inconnue' );
	} else {
		$valn1 = (int)$lastvals[0]->post_content;
		$valn0 = (int)$lastvals[1]->post_content;
		$date1 = date_create( $lastvals[0]->post_date_gmt );
		$date0 = date_create( $lastvals[1]->post_date_gmt );
		$diff  = date_diff($date1, $date0);
		$diffj = $diff->format('%d%');
		return number_format( ( ( ( $valn1 * 100 / $valn0 ) -100 ) / $diffj ), 2 ) . '%';
	}
}

function iv_get_1yeardelta( $partname, $lastvals ) {
	$stsylvestreval = get_posts( array( 
		                    'post_type' => 'value', 
		                    'tax_query' => array(
		                                         array(
			                                         'taxonomy' => 'type_value',
			                                         'field'    => 'slug',
			                                         'terms'    => $partname
			                                         )
		                                         ),
		                    'date_query' => array(
		                                          array(
		                                                'before'    => array(
															'year'  => date('Y'),
														),
													) 
		                                          ),
		                    'posts_per_page' => 1,
		                    'order'     => 'DESC'
		                    ) );
	// var_dump( $stsylvestreval );
	if ( ! isset( $lastvals[0] ) || ! isset( $stsylvestreval[0] ) ) {
		return __( 'inconnue' );
	} else {
		$valn1 = (int)$lastvals[0]->post_content;
		$valn0 = (int)$stsylvestreval[0]->post_content;
		return number_format( ( ( ( $valn1 * 100 / $valn0 ) -100 ) ), 2 ) . '%';
	}
}

function iv_get_totaldelta( $partname, $lastvals ) {
	$firstval = get_posts( array( 
		                    'post_type' => 'value', 
		                    'tax_query' => array(
		                                         array(
			                                         'taxonomy' => 'type_value',
			                                         'field'    => 'slug',
			                                         'terms'    => $partname
			                                         )
		                                         ),
		                    'posts_per_page' => 1,
		                    'order'     => 'ASC'
		                    ) );
	// var_dump( $stsylvestreval );
	if ( ! isset( $lastvals[0] ) || ! isset( $firstval[0] ) ) {
		return __( 'inconnue' );
	} else {
		$valn1 = (int)$lastvals[0]->post_content;
		$valn0 = (int)$firstval[0]->post_content;
		return number_format( ( ( ( $valn1 * 100 / $valn0 ) -100 ) ), 2 ) . '%';
	}
}



function iv_importeur( $data, $name ) {

	$newdata = array();
	$data = explode( "\n", $data );
	foreach( $data as $val ) {
		$val = explode( ',"', $val );
		$newdata[ $name ][ $val[0] ] = str_replace( array( '"', ' ', ',' ), array( '', '', '.'), $val[1] );
	}
	iv_insert_values( $newdata );
}
/*
add_action( 'init', 'I_import_now' );
function I_import_now() {
iv_importeur( '2012-08-28,"999,25"
2012-08-29,"996,97"
2012-08-30,"998,35"
2012-08-31,"998,67"
2012-09-03,"1002,33"
2012-09-04,"1001,89"
2012-09-05,"1000,49"
2012-09-06,"1002,8"
2012-09-07,"1006,37"
2012-09-10,"1007,11"
2012-09-11,"1006,96"
2012-09-12,"1010,19"
2012-09-13,"1012,83"
2012-09-14,"1020,43"
2012-09-17,"1022,86"
2012-09-18,"1018,28"
2012-09-19,"1016,66"
2012-09-20,"1014,73"
2012-09-21,"1015,36"
2012-09-24,"1014,92"
2012-09-25,"1014,39"
2012-09-26,"1010,08"
2012-09-27,"1010,68"
2012-09-28,"1011,67"
2012-10-01,"1014,16"
2012-10-02,"1015,4"
2012-10-03,"1015,73"
2012-10-04,"1017,19"
2012-10-05,"1020,98"
2012-10-08,"1015,89"
2012-10-09,"1013,18"
2012-10-10,"1010,88"
2012-10-11,"1010,96"
2012-10-12,"1011,73"
2012-10-15,"1011,13"
2012-10-16,"1015,02"
2012-10-17,"1018,69"
2012-10-18,"1019,27"
2012-10-19,"1015,21"
2012-10-22,"1012,9"
2012-10-23,"1007,58"
2012-10-24,"1004,17"
2012-10-25,"1006,92"
2012-10-26,"1005,61"
2012-10-30,"1005,1"
2012-10-31,"1007,05"
2012-11-01,"1008,87"
2012-11-02,"1007,99"
2012-11-05,"1008,73"
2012-11-06,"1011,58"
2012-11-07,"1011,37"
2012-11-08,"1005,11"
2012-11-09,"1003,31"
2012-11-12,"1003,13"
2012-11-13,"1000,88"
2012-11-14,"999,67"
2012-11-15,"994,51"
2012-11-16,"992,89"
2012-11-19,"997,07"
2012-11-20,"999,79"
2012-11-21,"1001,17"
2012-11-22,"1001,87"
2012-11-23,"1005,29"
2012-11-26,"1007,01"
2012-11-27,"1007,76"
2012-11-28,"1007,34"
2012-11-29,"1010,93"
2012-11-30,"1013,67"
2012-12-03,"1013,64"
2012-12-04,"1013,93"
2012-12-05,"1015,65"
2012-12-06,"1018,18"
2012-12-07,"1019,46"
2012-12-10,"1020,38"
2012-12-11,"1022,77"
2012-12-12,"1024,78"
2012-12-13,"1023,93"
2012-12-14,"1023,6"
2012-12-17,"1024,24"
2012-12-18,"1025,02"
2012-12-19,"1027,64"
2012-12-20,"1029,28"
2012-12-21,"1027,95"
2012-12-24,"1026,68"
2012-12-27,"1028,24"
2012-12-28,"1028,79"
2012-12-31,"1029,28"
2013-01-03,"1042,95"
2013-01-04,"1041,85"
2013-01-07,"1041,11"
2013-01-08,"1039,7"
2013-01-09,"1041,14"
2013-01-10,"1044,47"
2013-01-11,"1044,29"
2013-01-14,"1044,78"
2013-01-15,"1043,78"
2013-01-16,"1042,99"
2013-01-17,"1044,04"
2013-01-18,"1048,01"
2013-01-21,"1046,96"
2013-01-22,"1046,66"
2013-01-23,"1047,32"
2013-01-24,"1049,32"
2013-01-25,"1050,26"
2013-01-28,"1049,61"
2013-01-29,"1050,71"
2013-01-30,"1051,12"
2013-01-31,"1049,16"
2013-02-01,"1050,87"
2013-02-04,"1053,29"
2013-02-05,"1050,01"
2013-02-06,"1051,63"
2013-02-07,"1050,55"
2013-02-08,"1051,8"
2013-02-11,"1053,26"
2013-02-12,"1052,15"
2013-02-13,"1054,91"
2013-02-14,"1056,26"
2013-02-15,"1056,3"
2013-02-18,"1056,12"
2013-02-19,"1057,08"
2013-02-20,"1059,22"
2013-02-21,"1050,54"
2013-02-22,"1051,24"
2013-02-25,"1051,83"
2013-02-26,"1046,57"
2013-02-27,"1048,81"
2013-02-28,"1055,16"
2013-03-01,"1056,76"
2013-03-04,"1056,32"
2013-03-05,"1061,4"
2013-03-06,"1065,66"
2013-03-07,"1067,13"
2013-03-08,"1068,96"
2013-03-11,"1071,52"
2013-03-12,"1070,72"
2013-03-13,"1070,41"
2013-03-14,"1072,15"
2013-03-15,"1073,2"
2013-03-19,"1068,67"
2013-03-20,"1068,29"
2013-03-21,"1068,97"
2013-03-22,"1065,2"
2013-03-25,"1068,05"
2013-03-26,"1068,55"
2013-03-27,"1071,79"
2013-03-28,"1073,65"
2013-04-02,"1073,46"
2013-04-03,"1071,33"
2013-04-04,"1069,85"
2013-04-05,"1068,3"
2013-04-08,"1069,46"
2013-04-09,"1071,24"
2013-04-10,"1076,56"
2013-04-11,"1084,57"
2013-04-12,1084
2013-04-15,"1077,27"
2013-04-16,"1071,23"
2013-04-17,"1072,22"
2013-04-18,"1067,45"
2013-04-19,"1069,2"
2013-04-22,"1073,33"
2013-04-23,"1075,79"
2013-04-24,"1082,16"
2013-04-25,"1084,97"
2013-04-26,"1085,48"
2013-04-29,"1086,41"
2013-04-30,"1089,46"
2013-05-02,"1088,03"
2013-05-03,"1095,08"
2013-05-07,"1099,15"
2013-05-08,"1101,6"
2013-05-10,"1102,84"
2013-05-13,"1103,68"
2013-05-14,"1105,45"
2013-05-15,"1107,93"
2013-05-16,"1108,82"
2013-05-17,"1110,15"
2013-05-21,"1111,21"
2013-05-22,"1110,72"
2013-05-23,"1098,83"
2013-05-24,"1096,7"
2013-05-28,"1098,57"
2013-05-29,"1094,62"
2013-05-30,"1090,69"
2013-05-31,"1084,97"
2013-06-04,"1082,15"
2013-06-05,"1077,48"
2013-06-06,"1072,12"
2013-06-07,"1074,99"
2013-06-10,"1076,12"
2013-06-11,"1070,13"
2013-06-12,1066
2013-06-13,"1064,14"
2013-06-14,"1069,96"
2013-06-17,"1073,78"
2013-06-18,"1074,71"
2013-06-19,"1072,71"
2013-06-20,"1059,61"
2013-06-21,"1048,14"
2013-06-24,"1039,01"
2013-06-25,"1037,49"
2013-06-26,"1044,37"
2013-06-27,"1051,83"
2013-06-28,"1056,76"
2013-07-01,"1058,38"
2013-07-02,"1062,28"
2013-07-03,"1059,74"
2013-07-04,"1061,78"
2013-07-05,"1064,86"
2013-07-08,"1067,12"
2013-07-09,"1070,54"
2013-07-10,"1072,01"
2013-07-11,"1078,07"
2013-07-12,"1082,46"
2013-07-15,"1084,21"
2013-07-16,"1084,46"
2013-07-17,"1083,64"
2013-07-18,"1087,31"
2013-07-19,"1089,86"
2013-07-22,"1090,05"
2013-07-23,1090
2013-07-24,"1088,52"
2013-07-25,"1085,74"
2013-07-26,"1084,72"
2013-07-29,"1082,83"
2013-07-30,"1082,03"
2013-07-31,"1082,57"
2013-08-02,"1093,41"
2013-08-06,"1092,94"
2013-08-07,"1088,19"
2013-08-08,"1086,8"
2013-08-09,"1089,65"
2013-08-12,"1089,98"
2013-08-13,"1091,33"
2013-08-14,"1091,23"
2013-08-15,"1087,33"
2013-08-16,"1082,96"
2013-08-19,"1078,57"
2013-08-20,"1074,61"
2013-08-21,"1073,52"
2013-08-22,"1074,44"
2013-08-23,"1079,06"
2013-08-27,"1075,24"
2013-08-28,1066
2013-08-29,"1068,12"
2013-08-30,"1068,43"
2013-09-02,"1069,75"
2013-09-03,"1071,83"
2013-09-04,"1074,09"
2013-09-06,"1079,08"
2013-09-09,"1082,15"
2013-09-10,"1087,34"
2013-09-11,"1091,82"
2013-09-12,"1091,71"
2013-09-13,"1091,17"
2013-09-16,"1095,13"
2013-09-17,"1096,93"
2013-09-18,"1100,07"
2013-09-19,"1105,83"
2013-09-20,"1104,95"
2013-09-23,"1100,97"
2013-09-24,"1097,95"
2013-09-25,"1098,52"
2013-09-26,"1098,53"
2013-09-27,"1098,72"
2013-09-30,1095
2013-10-01,"1095,67"
2013-10-02,"1099,22"
2013-10-03,"1097,14"
2013-10-04,"1094,96"
2013-10-07,"1094,39"
2013-10-08,"1091,06"
2013-10-09,"1086,6"
2013-10-10,"1089,67"
2013-10-11,"1097,37"
2013-10-14,"1099,44"
2013-10-15,"1101,06"
2013-10-16,"1102,1"
2013-10-17,"1106,96"
2013-10-18,"1112,5"
2013-10-21,"1115,25"
2013-10-22,"1116,31"
2013-10-23,"1115,6"
2013-10-24,"1113,75"
2013-10-25,"1114,98"
2013-10-29,"1116,24"
2013-10-30,"1117,63"
2013-10-31,"1115,11"
2013-11-01,"1114,44"
2013-11-04,"1114,84"
2013-11-05,"1114,57"
2013-11-06,"1113,31"
2013-11-07,"1109,58"
2013-11-08,"1105,99"
2013-11-11,"1107,65"
2013-11-12,"1108,27"
2013-11-13,"1108,26"
2013-11-14,"1112,3"
2013-11-15,"1115,98"
2013-11-18,"1117,24"
2013-11-19,"1116,42"
2013-11-20,"1114,2"
2013-11-21,"1113,64"
2013-11-22,"1116,36"
2013-11-25,"1118,59"
2013-11-26,"1118,98"
2013-11-27,"1119,86"
2013-11-28,"1121,73"
2013-11-29,"1122,06"
2013-12-02,"1121,52"
2013-12-03,"1119,21"
2013-12-04,"1114,62"
2013-12-05,"1112,89"
2013-12-06,"1113,09"
2013-12-09,"1117,34"
2013-12-10,"1117,75"
2013-12-11,"1114,86"
2013-12-12,"1109,51"
2013-12-13,"1106,8"
2013-12-16,"1108,45"
2013-12-17,"1109,54"
2013-12-18,"1112,54"
2013-12-19,"1116,02"
2013-12-20,"1117,88"
2013-12-23,"1122,45"
2013-12-24,"1126,08"
2013-12-27,"1128,73"
2013-12-30,"1129,77"
2013-12-31,"1131,91"
2014-01-03,"1126,85"
2014-01-06,"1126,59"
2014-01-07,"1126,18"
2014-01-08,"1129,91"
2014-01-09,"1131,15"
2014-01-10,"1131,53"
2014-01-13,"1132,79"
2014-01-14,"1130,23"
2014-01-15,"1136,51"
2014-01-16,"1137,6"
2014-01-17,"1138,17"
2014-01-20,"1137,37"
2014-01-21,"1137,67"
2014-01-22,"1138,5"
2014-01-23,"1136,97"
2014-01-24,"1124,89"
2014-01-27,"1111,32"
2014-01-28,"1110,31"
2014-01-29,"1111,68"
2014-01-30,"1109,36"
2014-01-31,"1110,55"
2014-02-03,"1103,03"', 'IE00B7L3NC99.IR' );
}
//*/



